<?php
class App{
    public function run(){
        $developmentMode = true;

        $loader = new Twig_Loader_Filesystem(__DIR__ . '/../templates');
        $twig = new Twig_Environment($loader,
            [
                'debug' => $developmentMode,
                'cache' => __DIR__ . '/cache',
                'strict_variables' => $developmentMode
            ]
        );

        $user = [
            'name' => 'Fabien',
            'age' => 39
        ];

        $user = new stdClass();
        $user->name = "Pankracy";
        $user->age = "-8";

        $hobbies = array('hh','gddg','gfdgd');

        $result = $twig->render('hello.html.twig', array('user' => $user,'hobby'=> $hobbies));
        return $result;
    }
}