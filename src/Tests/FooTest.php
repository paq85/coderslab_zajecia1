<?php

require_once __DIR__ . '/../../vendor/autoload.php';

class FooTest extends PHPUnit_Framework_TestCase
{
    public function testFoo(){
        $app = new App();
        $result = $app->run();
        $this->assertContains("Hello Pankracy!", $result);
    }
}